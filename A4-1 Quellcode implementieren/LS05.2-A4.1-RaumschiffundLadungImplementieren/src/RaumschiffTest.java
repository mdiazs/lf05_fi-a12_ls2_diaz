
public class RaumschiffTest {

	public RaumschiffTest() {
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Ladung f�r Klingonen erzeugen
		Ladung ld1 = new Ladung();
		ld1.setBezeichnung("Ferengi Schneckensaft");
		ld1.setMenge(200);
		
		Ladung ld2 = new Ladung();
		ld2.setBezeichnung("Bet'leth Klingonen Schwert");
		ld2.setMenge(200);
		
		// Raumschiff f�r Klingonen erzeugen und beladen
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		klingonen.addLadung(ld1);
		klingonen.addLadung(ld2);
		
		
		// Ladung f�r Romulaner erzeugen 
		Ladung ld3 = new Ladung();
		ld3.setBezeichnung("Borg-Schrott");
		ld3.setMenge(5);
		
		Ladung ld4 = new Ladung();
		ld4.setBezeichnung("Rote Materie");
		ld4.setMenge(2);
		
		Ladung ld5 = new Ladung();
		ld5.setBezeichnung("Plasma-Waffe");
		ld5.setMenge(50);
		
		// Raumschiff f�r Romulaner erzeugen und beladen
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		romulaner.addLadung(ld3);
		romulaner.addLadung(ld4);
		romulaner.addLadung(ld5);
		
		
		// Ladung f�r Vulkanier erzeugen
		Ladung ld6 = new Ladung();
		ld6.setBezeichnung("Forschunssonde");
		ld6.setMenge(35);
		
		Ladung ld7 = new Ladung();
		ld7.setBezeichnung("Photonentorpedo");
		ld7.setMenge(3);
		
		// Raumschiff f�r Vulkanier erzeugen und beladen
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		vulkanier.addLadung(ld6);
		vulkanier.addLadung(ld7);
		
		// Quellcode verwenden und �berpr�fen
		
		klingonen.photonentorpedosSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		vulkanier.reparaturDurchfuehren(true, true, true, vulkanier.getAndroidenAnzahl());
		vulkanier.photonentorpedosLaden(ld7.getMenge());
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.photonentorpedosSchiessen(romulaner);
		klingonen.photonentorpedosSchiessen(romulaner);
		
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
				
		klingonen.eintraegeLogbuchZurueckgeben();
		romulaner.eintraegeLogbuchZurueckgeben();
		vulkanier.eintraegeLogbuchZurueckgeben();
		
		

	}

}
