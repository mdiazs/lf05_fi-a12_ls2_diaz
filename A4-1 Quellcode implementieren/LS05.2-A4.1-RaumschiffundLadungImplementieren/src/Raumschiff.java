/**
 * @author Maria Luisa Diaz, Klasse FI-A 12
 * 
 * @since Version 1.0
 * 
 * @see java.util.ArrayList, @see java.util.Random
 */

import java.util.ArrayList;
import java.util.Random;


/**
 * Klasse Raumschiff.
 * Definiert Attribute und Methoden f�r die Klasse.
 */
public class Raumschiff {
	
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	
	/**
	 * Parameterlose Konstruktor-Methode
	 * 
	 * @return Objekt der Klasse Raumschiff
	 */
	public Raumschiff() {
		
	}
	
	/**
	 * Vollparametrisierte Konstruktor-Methode
	 * 
	 * @param photonentorpedoAnzahl
	 * 				Anzahl Photonentorpedos, Integer
	 * @param energieversorgungInProzent
	 * 				Energieversorgung, Prozentualwert (Integer)
	 * @param schildeInProzent
	 * 				Schilde, Prozentualwert (Integer)
	 * @param huelleInProzent
	 * 				H�lle, Prozentualwert (Integer)
	 * @param lebenserhaltungssystemeInProzent
	 * 				Lebenserhaltungssysteme, Prozentualwert (Integer)
	 * @param androidenAnzahl
	 * 				Anzahl Reparaturandroiden (Integer)
	 * @param schiffsname
	 * 				Bezeichnung f�r den Schiff (Zeichenkette)
	 * @return Objekt der Klasse Raumschiff
	 */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {		
		
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	
	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	
	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}
	
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	public String getSchiffsname() {
		return this.schiffsname;
	}
	
	/**
	 * @param neueLadung
	 * 				Objektbezeichner der Klasse Ladung
	 * @return Objekt wird zum Array ladungsverzeichnis hinzugef�gt
	 */
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}

	/**
	 * Falls der angreifende Raumschiff keine Photonentorpedos besitzt,
	 * wird die Nachricht -=*Click*=- an alle geschickt.
	 * Andernfalls wird einen Photonentorpedo geschossen, den Treffer vermerkt und eine Nachricht an alle geschickt.
	 * 
	 * @param r
	 * 				Objektbezeichner eines Objekts der Klasse raumschiff
	 * @return Eine String mit unterschiedlichem Inhalt je nach dem, ob einen Photonentorpedo geschossen wurde, die Anzahl verf�gbaren Photonentorpedos wird entsprechend aktualisiert.
	 */
	public void photonentorpedosSchiessen(Raumschiff r) {
		if (this.getPhotonentorpedoAnzahl() == 0) { // wenn es keine Photonentorpedos gibt,
			nachrichtAnAlle("-=*Click*=-"); // wird die Nachricht -=*Click*=- an alle �bergeben

		} else {
			int anzahlPhotonentorpedos = this.getPhotonentorpedoAnzahl(); // wenn es Photonentorpedos gibt,
			anzahlPhotonentorpedos -= 1; // Anzahl Photonentorpedos wird um 1 verringert,
			this.setPhotonentorpedoAnzahl(anzahlPhotonentorpedos); // Anzahl Photonentorpedos wird aktualisiert,

			nachrichtAnAlle("Photonentorpedo abgeschossen"); // eine Nachricht wird an alle verschickt,
			treffer(r); // und der Treffer wird vermerkt.
		}
	}

	/**
	 * Falls der angreifende Raumschiff keine ausreichende Energieversorung hat,
	 * wird die Nachricht -=*Click*=- an alle geschickt.
	 * Andernfalls wird die Phaserkanone abgeschossen, den Treffer vermerkt und eine Nachricht an alle geschickt.
	 * 
	 * @param r
	 * 				Objektbezeichner eines Objekts der Klasse raumschiff
	 * @return Eine String mit unterschiedlichem Inhalt je nach dem, ob die Phaserkanone abgeschossen wurde, die Energieversorgung wird entsprechend aktualisiert.
	 */
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (this.getEnergieversorgungInProzent() < 50) { // wenn die Energieversorgungsprozent unter 50% liegt,
			nachrichtAnAlle("-=*Click*=-"); // wird die Nachricht -=*Click*=- an alle geschickt.

		} else {
			this.setEnergieversorgungInProzent(this.getEnergieversorgungInProzent() - 50); // wenn die Energieversorgung �ber 50% liegt, wird die Energieversorgung um 50% verringert,

			nachrichtAnAlle("Phaserkanone abgeschossen"); // es wird eine Nachricht an alle geschickt,
			treffer(r); // und der Treffer wird vermerkt.
			
		}
	}
	
	/**
	 * Falls einen Treffer vermerkt wurde,
	 * wird eine Nachricht an alle verschickt,
	 * der Schild des getroffenen Raumschiffes wird um 50% verringert.
	 * Falls der Schild dabei zerst�rt wird, werden H�lle und Energieversorgung jeweils um 50% verringert.
	 * 
	 * Ist die H�lle ebenfalls zest�rt, werden die Lebenserhaltungssysteme vernichtet (auf Null gesetzt) und eine Nachricht an alle verschickt.
	 * 
	 * @param r
	 * 				Objektbezeichner eines Objekts der Klasse raumschiff
	 * 
	 * @return Je nach Stand des Schildes und der H�lle vom getroffenen Raumschiff, werden die Werte verringert oder komplett zest�rt, eine Nachricht wird an alle verschickt.
	 */
	private void treffer(Raumschiff r) {
		System.out.println(r.getSchiffsname() + " wurde getroffen!"); // Es wird in der Konsole ausgegeben, dass einen Raumschiff getroffen wurde.
		
		r.setSchildeInProzent(r.getSchildeInProzent() - 50); // Die Schilde des getroffenen Raumschiffes wird um 50% verringert.
		
		if (r.getSchildeInProzent() <= 0) { // Wenn die Schilde des getroffenen Raumschiffes zerst�rt wurden (liegen bei 0% oder weniger),
			r.setHuelleInProzent(r.getHuelleInProzent() - 50); // wird die H�lle ebenfalls um 50% verringert,
			r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent() - 50); // und die Energieversorgung auch um 50% verringert.
			
			if (r.getHuelleInProzent() <= 0) { // Falls die H�lle dabei zest�rt wurde (liegt bei 0% oder weniger),
				r.setLebenserhaltungssystemeInProzent(0); // wird das Lebenserhaltungssystem vernichtet (auf Null gesetzt),
				nachrichtAnAlle("Lebenserhaltungssysteme des Raumschiffes " + r.getSchiffsname() + " wurden vernichtet."); // und eine Nachricht an alle geschickt.
			}
		}
		
		if (r.getHuelleInProzent() <= 0) { // Wenn die H�lle des getroffenen Raumschiffes zerst�rt wurden (liegt bei 0% oder weniger),
			r.setLebenserhaltungssystemeInProzent(0); // wird das Lebenserhaltungssystem vernichtet (auf Null gesetzt),
			nachrichtAnAlle("Lebenserhaltungssysteme des Raumschiffes " + r.getSchiffsname() + " wurden vernichtet."); // und eine Nachricht an alle geschickt.
		}
			
		
	}
	
	/**
	 * Nachrichten, die an alle geschickt wurden, werden in der Konsole ausgegeben und zu einem Broadcastkommunikator eingef�gt.
	 * @param message
	 * 				Nachricht, die von einem Raumschiff gesendet wurde, Zeichenkette.
	 * 
	 * @return Die �bergebene Zeichenkette wird dem Broadcastkommunikator hinzugef�gt.
	 */
	public void nachrichtAnAlle (String message) {
		System.out.println(message);
		this.broadcastKommunikator.add(message);
	}
	
	/**
	 * Gibt alle gespeicherte Nachrichten (Zeichenketten) aus dem Broadcastkommunikator zur�ck.
	 * 
	 * @return Inhalt des Broadcastkommunikators (alle Nachrichten, die im Broadcastkommunikator gespeichert wurden).
	 */
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return this.broadcastKommunikator;
	}
	
	/**
	 * �berpr�ft, ob Photonentorpedos im Ladungsverzeichnis vorhanden sind.
	 * Falls ja, werden die Photonentorpedos geladen.
	 * Falls nein, wird eine Meldung in der Konsole ausgegeben und eine Nachricht an alle verschickt.
	 * 
	 * @param anzahlTorpedos
	 * 				Anzahl Photonentorpedos, die aus der Ladung geholt werden sollen und geladen werden sollen.
	 * 
	 * @return Bei erfolgreicher Ladung der Photonentorpedos wird die Anzahl verf�gbare Photonentorpedos entsprechend aktualisiert. Sind keine Photonentorpedos vorhanden, wird eine Meldung in der Konsole ausgegeben und eine Nachricht an alle geschickt.
	 */
	public void photonentorpedosLaden (int anzahlTorpedos) {
		boolean sindTorpedosVorhanden = false; // ein Boolean wird mit False initialisiert und kontrolliert, ob Photonentorpedos in der Ladung vorhanden sind.
		int anzahlTorpedosInLadung = 0; // die Anzahl Torpedos aus der Ladung wird mit einer Kontroll-Variable protokolliert.
		int indexTorpedos = 0; // ein Index wird initialisiert, um dann durch das Ladungsverzeichnis zu iterieren.
		for (int i = 0; i < this.ladungsverzeichnis.size(); i++) { // es wird durch alle Elemente im Ladungsverzeichnis iteriert.
			if (this.ladungsverzeichnis.get(i).getBezeichnung().equals("Photonentorpedo")) { // ist die Bezeichnung des Elements im Ladungsverzeichnis GLEICH Photonentorpedo,
				anzahlTorpedosInLadung = this.ladungsverzeichnis.get(i).getMenge(); // protokolliere die Menge des Elements, 
				sindTorpedosVorhanden = true; // merke, dass Photonentorpedos in der Ladung vorhanden sind (setze das Kontroll-Boolean auf true),
				indexTorpedos = i; // merke au�erdem, welche Position die Torpedos im Ladungsverzeichnis haben.
			}
		}
		
		if (sindTorpedosVorhanden == false) { // ist das Kontroll-Boolean gleich geblieben (es wurden keine Torpedos im Ladungsverzeichnis gefunden),
			System.out.println("Keine Torpedos gefunden"); // gebe eine Meldung in der Konsole aus,
			this.nachrichtAnAlle("-=*Click*=-"); // und sende eine Nachricht an alle.
		} else if (sindTorpedosVorhanden == true && anzahlTorpedosInLadung < anzahlTorpedos ) { // sind Torpedos vorhanden, und die Anzahl verf�gbare Torpedos ist KLEINER ALS die Anzahl nachgefragte Torpedos,
			this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() + anzahlTorpedosInLadung); // aktualisiere die Anzahl einsatzbereite Torpedos (addiere die Anzahl Torpedos, die im Ladungsverzeichnis gefunden wurden),
			this.ladungsverzeichnis.get(indexTorpedos).setMenge(0); // aktualisiere das Ladungsverzeichnis, da wir nun keine Torpedos mehr haben (alle verf�gbaren wurden eingesetzt),
			System.out.println(anzahlTorpedosInLadung + " Photonentorpedo(s) eingesetzt."); // und gebe in der Konsole aus, wieviele Torpedos eingesetzt wurden.
		} else if (sindTorpedosVorhanden == true && anzahlTorpedosInLadung >= anzahlTorpedos) { // sind Torpedos vorhanden, und die Anzahl verf�gbare Torpedos ist GR�SSER ALS oder GLEICH ALS die Anzahl nachgefragte Torpedos,
			this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() + anzahlTorpedosInLadung); // aktualisiere die Anzahl einsatzbereite Torpedos (addiere die Anzahl Torpedos, die im Ladungsverzeichnis gefunden wurden),
			this.ladungsverzeichnis.get(indexTorpedos).setMenge(anzahlTorpedosInLadung - anzahlTorpedos); // aktualisiere das Ladungsverzeichnis, subtrahiere davon die Anzahl Torpedos, die eingesetzt wurden,
			System.out.println(anzahlTorpedosInLadung + " Photonentorpedo(s) eingesetzt."); // und gebe in der Konsole aus, wieviele Torpedos eingesetzt wurden.
		}
			
		}
	
	/**
	 * �berpr�ft, welche Raumschiffsstrukturen repariert werden sollen und wie viele Reparaturandroiden daf�r eingesetzt werden sollen.
	 * Generiert eine Zufallszahl f�r die Reparatur.
	 * Repariert die Strukturen, die der Methode �bergeben wurden.
	 * 
	 * @param schutzschilde
	 * 				Sollen die Schilde repariert werden? True = Ja, False = Nein
	 * 
	 * @param energieversorgung
	 * 				Soll die Energieversorgung repariert werden? True = Ja, False = Nein
	 * 
	 * @param schiffshuelle
	 * 				Soll die H�lle repariert werden? True = Ja, False = Nein
	 * 
	 * @param anzahlDroiden
	 * 				Anzahl Androiden, die f�r die Reparatur eingesetzt werden sollen
	 * 
	 * @return Ein Reparaturwert, die den zu reparierenden Strukturen eingef�gt wird.
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		Random z = new Random(); // es wird ein Objekt der Klasse Random erstellt.
		int min = 0; // Min. Grenze zur Berechnung einer Zufallszahl
		int max = 100; // Max. Grenze zur Berechnung einer Zufallszahl
		
		int zufallszahl = z.nextInt(max - min) + min; // Berechnung einer Zufallszahl
		
		if (anzahlDroiden > this.getAndroidenAnzahl()) { // �berpr�ft, ob die Anzahl Androiden, die eingesetzt werden sollen GR�SSER IST als die Anzahl Androiden, die im Raumschiff vorhanden sind.
			anzahlDroiden = this.getAndroidenAnzahl(); // wenn ja, werden einfach alle verf�gbaren Androiden eingesetzt.
		}
		
		int strukturenAufTrue = 0; // Kontroll-Variable, protokolliert wie viele Strukturen repariert werden sollen
		
		if (schutzschilde == true) { // Es wird jede Boolean f�r jede Struktur gepr�ft. Wenn das Boolean True ist,
			strukturenAufTrue += 1; // wird die Kontroll-Variable um 1 inkrementiert.
		}
		
		if (energieversorgung == true) {
			strukturenAufTrue += 1;
		}
		
		if (schiffshuelle == true) {
			strukturenAufTrue += 1;
		}
		
		int reparatur = (zufallszahl * anzahlDroiden) / strukturenAufTrue; // Berechnung des Reparaturwerts
		
		if (schutzschilde == true) { // Strukturen werden nochmal einzeln gepr�ft. Wenn sie repariert werden sollen,
			this.setSchildeInProzent(this.getSchildeInProzent() + reparatur); // wird der Strukturwert um den Reparaturwert inkrementiert.
		}
		
		if (energieversorgung == true) {
			this.setEnergieversorgungInProzent(this.getEnergieversorgungInProzent() + reparatur);
		}
		
		if (schiffshuelle == true) {
			this.setHuelleInProzent(this.getHuelleInProzent() + reparatur);
		}
	}

	/**
	 * Gibt den Zustand des Raumschiffes in der Konsole aus.
	 * 
	 * @return Mehrere Zeichenketten, die jeweils den Stand eines Attributes in der Konsole ausgeben.
	 */
	public void zustandRaumschiff() {
		System.out.println("Zust�nde des Raumschiffs " + this.getSchiffsname() + ":");
		System.out.println("Anzahl Photonentorpedos: " + this.getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung: " + this.getEnergieversorgungInProzent() + "%"); // Prozentualwerte werden mit einem % formatiert.
		System.out.println("Schilde: " + this.getSchildeInProzent() + "%");
		System.out.println("H�lle: " + this.getHuelleInProzent() + "%");
		System.out.println("Lebenserhaltungssystem: " + this.getLebenserhaltungssystemeInProzent() + "%");
		System.out.println("Anzahl Androiden: " + this.getAndroidenAnzahl() + "\n"); // Der letzten Ausgabe wird eine Linie hinzugef�gt, zur besseren Lesbarkeit der Konsole.
	}
	
	/**
	 * Gibt den Inhalt des Ladungsverzeichnisses in der Konsole aus.
	 * 
	 * @return Mehrere Zeichenketten mit der Bezeichnung und der Menge jeder vorhandenen Ladung.
	 */
	public void ladungsverzeichnisAusgeben() {
		System.out.println("Ladungsverzeichnis des " + this.getSchiffsname() + ":"); // Eine Zeichenkette markiert den Anfang der Ausgabe, zur besseren Lesbarkeit.
		
		for (int i = 0; i < this.ladungsverzeichnis.size(); i++) { // Es wird durch alle Elemente im Ladungsverzeichnis iteriert.
			System.out.println(this.ladungsverzeichnis.get(i).toString()); // bei jeder Iteration soll die entsprechende Ladungs als Zeichenkette ausgegeben werden.
		}
		System.out.print("\n"); // Am Ende der Ausgabe wird eine leere Linie hinzugef�gt, zur besseren Lesbarkeit der Konsole.
	}
	/**
	 * R�umt das Ladungsverzeichnis aus, die Elemente werden gel�scht.
	 * 
	 * @return Ladungsverzeichnis wird geleert, die Elemente werden gel�scht.
	 */
	public void ladungsverzeichnisAufraeumen() {
		for (int i = 0; i < this.ladungsverzeichnis.size(); i++) { // Es wird durch alle Elemente im Ladungsverzeichnis iteriert,
			if (this.ladungsverzeichnis.get(i).getMenge() == 0) { // die Menge jedes Elementes wird auf Null gesetzt.
				this.ladungsverzeichnis.remove(i); // das Element wird aus dem Ladungsverzeichnis entfernt.
			}
		}
	}
}
