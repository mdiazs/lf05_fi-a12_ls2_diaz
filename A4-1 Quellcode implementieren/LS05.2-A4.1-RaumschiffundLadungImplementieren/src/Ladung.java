/**
 * @author Maria Luisa Diaz, Klasse FI-A 12
 * 
 * @since Version 1.0
 * 
 */

public class Ladung {

	private String bezeichnung;
	private int menge;
	
	
	/**
	 * Parameterlose Konstruktor-Methode
	 * 
	 * @return Objekt der Klasse Ladung
	 */
	public Ladung() {
	}

	/**
	 * Vollparametrisierte Konstruktor-Methode
	 * 
	 * @param bezeichnung
	 * 				Bezeichnung oder Name der Ladung, Zeichenkette
	 * @param menge
	 * 				Menge oder Anzahl der Ladung, Integer
	 * 
	 * @return Objekt der Klasse Ladung
	 */
	public Ladung (String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	public void setBezeichnung(String name) {
		this.bezeichnung = name;
	}
	
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	public int getMenge() {
		return this.menge;
	}	
	
	/**
	 * Gibt das erzeugte Objekt als Zeichenkette zur�ck, mit einer entsprechenden Formatierung.
	 * 
	 * @return Zeichenkette, die Informationen zum Objekt enth�lt, entsprechend formatiert.
	 */
	@Override
	public String toString() {
		return this.bezeichnung + ": " + this.menge; // die Zeichenkette enth�lt die Bezeichnung und die Menge des jeweiligen Objekts.
	}
	
}
